EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5FE4ACCA
P 750 900
F 0 "#FLG01" H 750 975 50  0001 C CNN
F 1 "PWR_FLAG" H 750 1073 50  0000 C CNN
F 2 "" H 750 900 50  0001 C CNN
F 3 "~" H 750 900 50  0001 C CNN
	1    750  900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5FE4BC1A
P 750 1200
F 0 "#PWR01" H 750 950 50  0001 C CNN
F 1 "GND" H 755 1027 50  0000 C CNN
F 2 "" H 750 1200 50  0001 C CNN
F 3 "" H 750 1200 50  0001 C CNN
	1    750  1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  900  750  1200
Text Label 1200 1100 0    50   ~ 0
5V
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5FECB6B8
P 1200 1200
F 0 "#FLG02" H 1200 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 1200 1373 50  0000 C CNN
F 2 "" H 1200 1200 50  0001 C CNN
F 3 "~" H 1200 1200 50  0001 C CNN
	1    1200 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 900  1200 1200
$Comp
L power:+12V #PWR03
U 1 1 5FF685DA
P 1600 900
F 0 "#PWR03" H 1600 750 50  0001 C CNN
F 1 "+12V" H 1615 1073 50  0000 C CNN
F 2 "" H 1600 900 50  0001 C CNN
F 3 "" H 1600 900 50  0001 C CNN
	1    1600 900 
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5FF68AA9
P 1600 1200
F 0 "#FLG03" H 1600 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 1373 50  0000 C CNN
F 2 "" H 1600 1200 50  0001 C CNN
F 3 "~" H 1600 1200 50  0001 C CNN
	1    1600 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 900  1600 1200
$Comp
L power:+5V #PWR02
U 1 1 5FF7757A
P 1200 900
F 0 "#PWR02" H 1200 750 50  0001 C CNN
F 1 "+5V" H 1215 1073 50  0000 C CNN
F 2 "" H 1200 900 50  0001 C CNN
F 3 "" H 1200 900 50  0001 C CNN
	1    1200 900 
	1    0    0    -1  
$EndComp
Text Label 1600 1100 0    50   ~ 0
VBAT
Text Label 1600 3900 2    50   ~ 0
VBAT
$Comp
L Device:C C1
U 1 1 6151B121
P 1750 4250
F 0 "C1" V 1498 4250 50  0000 C CNN
F 1 "10 uF" V 1589 4250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 1788 4100 50  0001 C CNN
F 3 "~" H 1750 4250 50  0001 C CNN
F 4 "C15850" V 1750 4250 50  0001 C CNN "JLC"
	1    1750 4250
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 615254EF
P 1750 4600
F 0 "#PWR04" H 1750 4350 50  0001 C CNN
F 1 "GND" H 1755 4427 50  0000 C CNN
F 2 "" H 1750 4600 50  0001 C CNN
F 3 "" H 1750 4600 50  0001 C CNN
	1    1750 4600
	1    0    0    -1  
$EndComp
Connection ~ 2750 4500
Wire Wire Line
	2850 4500 2750 4500
$Comp
L Regulator_Switching:TPS5430DDA U1
U 1 1 614FEC40
P 2850 4100
F 0 "U1" H 2850 4567 50  0000 C CNN
F 1 "TPS5430DDA" H 2850 4476 50  0000 C CNN
F 2 "Package_SO:TI_SO-PowerPAD-8_ThermalVias" H 2900 3750 50  0001 L CIN
F 3 "http://www.ti.com/lit/ds/symlink/tps5430.pdf" H 2850 4100 50  0001 C CNN
F 4 "C9864" H 2850 4100 50  0001 C CNN "JLC"
	1    2850 4100
	1    0    0    -1  
$EndComp
NoConn ~ 2350 4300
$Comp
L Device:C C2
U 1 1 6155743A
P 3700 3900
F 0 "C2" V 3448 3900 50  0000 C CNN
F 1 "10 nF" V 3539 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3738 3750 50  0001 C CNN
F 3 "~" H 3700 3900 50  0001 C CNN
F 4 "C1710" V 3700 3900 50  0001 C CNN "JLC"
	1    3700 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 3900 3550 3900
Wire Wire Line
	3850 3900 4000 3900
Wire Wire Line
	4000 3900 4000 4100
Wire Wire Line
	4000 4100 3350 4100
$Comp
L Device:L_Core_Iron L1
U 1 1 61571258
P 4900 4100
F 0 "L1" V 5125 4100 50  0000 C CNN
F 1 "15 μH" V 5034 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_Abracon_ASPI-0630LR" H 4900 4100 50  0001 C CNN
F 3 "https://products.sumida.com/ProductsInfo/ProductGuide/PowerInductors/TypeDetail.php?type=0630CDMCD%2FDS" H 4900 4100 50  0001 C CNN
	1    4900 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4000 4100 4450 4100
Connection ~ 4000 4100
$Comp
L power:+5V #PWR05
U 1 1 6159CF30
P 5900 3900
F 0 "#PWR05" H 5900 3750 50  0001 C CNN
F 1 "+5V" H 5915 4073 50  0000 C CNN
F 2 "" H 5900 3900 50  0001 C CNN
F 3 "" H 5900 3900 50  0001 C CNN
	1    5900 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 615AF711
P 5900 4400
F 0 "R1" H 5970 4446 50  0000 L CNN
F 1 "10 KΩ" H 5970 4355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5830 4400 50  0001 C CNN
F 3 "~" H 5900 4400 50  0001 C CNN
F 4 "C17414" H 5900 4400 50  0001 C CNN "JLC"
	1    5900 4400
	1    0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 615B177D
P 5900 4850
F 0 "R2" H 5970 4896 50  0000 L CNN
F 1 "3.24 KΩ" H 5970 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 5830 4850 50  0001 C CNN
F 3 "~" H 5900 4850 50  0001 C CNN
F 4 "C26010" H 5900 4850 50  0001 C CNN "JLC"
	1    5900 4850
	1    0    0    1   
$EndComp
Wire Wire Line
	2750 5050 2750 4500
Wire Wire Line
	5900 4550 5900 4650
Wire Wire Line
	5900 4250 5900 4100
Connection ~ 5900 4100
Wire Wire Line
	5900 4650 3350 4650
Wire Wire Line
	3350 4650 3350 4300
Wire Wire Line
	4450 4200 4450 4100
Connection ~ 4450 4100
Wire Wire Line
	4450 4100 4750 4100
$Comp
L Device:D_Schottky D1
U 1 1 615FFF03
P 4450 4350
F 0 "D1" V 4404 4430 50  0000 L CNN
F 1 "D_Schottky" V 4495 4430 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 4450 4350 50  0001 C CNN
F 3 "~" H 4450 4350 50  0001 C CNN
F 4 "C8678" V 4450 4350 50  0001 C CNN "JLC"
	1    4450 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 5050 4450 5050
Wire Wire Line
	4450 4500 4450 5050
Connection ~ 4450 5050
Wire Wire Line
	5900 5050 5900 5000
Wire Wire Line
	5900 4650 5900 4700
Connection ~ 5900 4650
$Comp
L Device:C C3
U 1 1 6151036A
P 5450 4400
F 0 "C3" V 5198 4400 50  0000 C CNN
F 1 "220 uF" V 5289 4400 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 5488 4250 50  0001 C CNN
F 3 "~" H 5450 4400 50  0001 C CNN
F 4 "C15008" V 5450 4400 50  0001 C CNN "JLC"
	1    5450 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5050 5450 5050
Wire Wire Line
	5050 4100 5450 4100
Wire Wire Line
	5450 4100 5450 4250
Connection ~ 5450 4100
Wire Wire Line
	5450 4100 5900 4100
Wire Wire Line
	5450 4550 5450 5050
Connection ~ 5450 5050
Wire Wire Line
	5450 5050 5900 5050
Text Label 5400 2050 0    50   ~ 0
VBAT
Wire Wire Line
	5400 2050 5200 2050
Text Label 750  1100 0    50   ~ 0
GND
Text Label 5400 2150 0    50   ~ 0
GND
Wire Wire Line
	5400 2150 5200 2150
Text Label 5400 2550 0    50   ~ 0
VBAT
Wire Wire Line
	5400 2550 5200 2550
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 6193E2D6
P 5000 2550
F 0 "J5" H 4892 2325 50  0000 C CNN
F 1 "Conn_01x01_Female" H 4892 2416 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 5000 2550 50  0001 C CNN
F 3 "~" H 5000 2550 50  0001 C CNN
	1    5000 2550
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 61997D5A
P 7950 1300
F 0 "J6" H 7978 1276 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 1185 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 1300 50  0001 C CNN
F 3 "~" H 7950 1300 50  0001 C CNN
	1    7950 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1300 7750 1200
Text Label 7600 1400 2    50   ~ 0
5V
Wire Wire Line
	7600 1400 7750 1400
Text Label 7600 1500 2    50   ~ 0
GND
Wire Wire Line
	7600 1500 7750 1500
$Comp
L Connector:Conn_01x04_Female J7
U 1 1 6199F2A1
P 7950 1750
F 0 "J7" H 7978 1726 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 1635 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 1750 50  0001 C CNN
F 3 "~" H 7950 1750 50  0001 C CNN
	1    7950 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1750 7750 1650
Text Label 7600 1850 2    50   ~ 0
5V
Wire Wire Line
	7600 1850 7750 1850
Text Label 7600 1950 2    50   ~ 0
GND
Wire Wire Line
	7600 1950 7750 1950
$Comp
L Connector:Conn_01x04_Female J8
U 1 1 6199FCB0
P 7950 2200
F 0 "J8" H 7978 2176 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 2085 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 2200 50  0001 C CNN
F 3 "~" H 7950 2200 50  0001 C CNN
	1    7950 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2200 7750 2100
Text Label 7600 2300 2    50   ~ 0
5V
Wire Wire Line
	7600 2300 7750 2300
Text Label 7600 2400 2    50   ~ 0
GND
Wire Wire Line
	7600 2400 7750 2400
$Comp
L Connector:Conn_01x04_Female J9
U 1 1 619A2B8B
P 7950 2650
F 0 "J9" H 7978 2626 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 2535 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 2650 50  0001 C CNN
F 3 "~" H 7950 2650 50  0001 C CNN
	1    7950 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2650 7750 2550
Text Label 7600 2750 2    50   ~ 0
5V
Wire Wire Line
	7600 2750 7750 2750
Text Label 7600 2850 2    50   ~ 0
GND
Wire Wire Line
	7600 2850 7750 2850
$Comp
L Connector:Conn_01x04_Female J10
U 1 1 619A2B96
P 7950 3100
F 0 "J10" H 7978 3076 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 2985 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 3100 50  0001 C CNN
F 3 "~" H 7950 3100 50  0001 C CNN
	1    7950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3100 7750 3000
Text Label 7600 3200 2    50   ~ 0
5V
Wire Wire Line
	7600 3200 7750 3200
Text Label 7600 3300 2    50   ~ 0
GND
Wire Wire Line
	7600 3300 7750 3300
$Comp
L Connector:Conn_01x04_Female J11
U 1 1 619A2BA1
P 7950 3550
F 0 "J11" H 7978 3526 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 3435 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 3550 50  0001 C CNN
F 3 "~" H 7950 3550 50  0001 C CNN
	1    7950 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3550 7750 3450
Text Label 7600 3650 2    50   ~ 0
5V
Wire Wire Line
	7600 3650 7750 3650
Text Label 7600 3750 2    50   ~ 0
GND
Wire Wire Line
	7600 3750 7750 3750
$Comp
L Connector:Conn_01x04_Female J12
U 1 1 619A6064
P 7950 4000
F 0 "J12" H 7978 3976 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 3885 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 4000 50  0001 C CNN
F 3 "~" H 7950 4000 50  0001 C CNN
	1    7950 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4000 7750 3900
Text Label 7600 4100 2    50   ~ 0
5V
Wire Wire Line
	7600 4100 7750 4100
Text Label 7600 4200 2    50   ~ 0
GND
Wire Wire Line
	7600 4200 7750 4200
$Comp
L Connector:Conn_01x04_Female J13
U 1 1 619A606F
P 7950 4450
F 0 "J13" H 7978 4426 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 4335 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 4450 50  0001 C CNN
F 3 "~" H 7950 4450 50  0001 C CNN
	1    7950 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4450 7750 4350
Text Label 7600 4550 2    50   ~ 0
5V
Wire Wire Line
	7600 4550 7750 4550
Text Label 7600 4650 2    50   ~ 0
GND
Wire Wire Line
	7600 4650 7750 4650
$Comp
L Connector:Conn_01x04_Female J14
U 1 1 619A607A
P 7950 4900
F 0 "J14" H 7978 4876 50  0000 L CNN
F 1 "Conn_01x04_Female" H 7978 4785 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 4900 50  0001 C CNN
F 3 "~" H 7950 4900 50  0001 C CNN
	1    7950 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4900 7750 4800
Text Label 7600 5000 2    50   ~ 0
5V
Wire Wire Line
	7600 5000 7750 5000
Text Label 7600 5100 2    50   ~ 0
GND
Wire Wire Line
	7600 5100 7750 5100
Text Label 5400 1600 0    50   ~ 0
VBAT
Wire Wire Line
	5400 1600 5200 1600
Text Label 5400 1700 0    50   ~ 0
GND
Wire Wire Line
	5400 1700 5200 1700
$Comp
L Connector:Conn_01x01_Female J1
U 1 1 619B6512
P 5000 1600
F 0 "J1" H 4892 1375 50  0000 C CNN
F 1 "Conn_01x01_Female" H 4892 1466 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5000 1600 50  0001 C CNN
F 3 "~" H 5000 1600 50  0001 C CNN
	1    5000 1600
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 619B6D02
P 5000 1700
F 0 "J2" H 4892 1475 50  0000 C CNN
F 1 "Conn_01x01_Female" H 4892 1566 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5000 1700 50  0001 C CNN
F 3 "~" H 5000 1700 50  0001 C CNN
	1    5000 1700
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 619B71FC
P 5000 2050
F 0 "J3" H 4892 1825 50  0000 C CNN
F 1 "Conn_01x01_Female" H 4892 1916 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5000 2050 50  0001 C CNN
F 3 "~" H 5000 2050 50  0001 C CNN
	1    5000 2050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 619B76E4
P 5000 2150
F 0 "J4" H 4892 1925 50  0000 C CNN
F 1 "Conn_01x01_Female" H 4892 2016 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x01_SMD_1x2mm" H 5000 2150 50  0001 C CNN
F 3 "~" H 5000 2150 50  0001 C CNN
	1    5000 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 3900 5900 4100
Text Label 5900 3950 0    50   ~ 0
5V
$Comp
L power:GND #PWR0101
U 1 1 618F613D
P 5900 5050
F 0 "#PWR0101" H 5900 4800 50  0001 C CNN
F 1 "GND" H 5905 4877 50  0000 C CNN
F 2 "" H 5900 5050 50  0001 C CNN
F 3 "" H 5900 5050 50  0001 C CNN
	1    5900 5050
	1    0    0    -1  
$EndComp
Connection ~ 5900 5050
Wire Wire Line
	1600 3900 1750 3900
Wire Wire Line
	1750 4100 1750 3900
Connection ~ 1750 3900
Wire Wire Line
	1750 4400 1750 4600
Wire Wire Line
	1750 3900 2350 3900
$EndSCHEMATC
