Manual FC
=========

This project contains a convenience board for manual planes.


## Images

Here are some images of the PCB. The SVGs are the most up to date:


![](misc/manualfc-top.svg)
![](misc/manualfc-bottom.svg)
